package com.example.thetowerofhanoiec;

import java.io.IOException;
import java.util.Stack;
import java.util.concurrent.Callable;

import org.andengine.engine.camera.Camera;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.bitmap.AssetBitmapTexture;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.texture.region.TextureRegionFactory;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.ui.activity.SimpleBaseGameActivity;

import android.util.Log;
import android.view.KeyEvent;

public class MainActivity extends SimpleBaseGameActivity {

    private static int CAMERA_WIDTH = 1024, CAMERA_HEIGHT = 600, level, state;
    
    private GameLevel gameLevel;
    
    private TextureRegion mTexRegBgSky;
    private TextureRegion mTexRegBgCloud;
    private TextureRegion mTexRegBgLand;
    private Font mFont;
    private TextureRegion mTexRegItemMenuPlay;
    private TextureRegion mTexRegItemMenuAbout;
    private TextureRegion mTexRegItemMenuExit;

    @Override
    public EngineOptions onCreateEngineOptions() {
//    	int level = 1;
        final Camera camera = new Camera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT);
        return new EngineOptions(
                true,
                ScreenOrientation.LANDSCAPE_FIXED,
                new RatioResolutionPolicy(CAMERA_WIDTH, CAMERA_HEIGHT),
                camera
        );
    }

    @Override
    protected void onCreateResources() {
        GameLevel.registerResources(this.getTextureManager(), this.getAssets(), this.getFontManager());
        
//        Menu.registerResources(this.getTextureManager(), this.getAssets(), this.getFontManager());
        try {
        	final ITexture texBgSky = new AssetBitmapTexture(this.getTextureManager(), this.getAssets(), "gfx/bg-sky.png");
        	this.mTexRegBgSky = TextureRegionFactory.extractFromTexture(texBgSky);
			texBgSky.load();

			final ITexture texBgCloud = new AssetBitmapTexture(this.getTextureManager(), this.getAssets(), "gfx/bg-cloud.png");
			this.mTexRegBgCloud = TextureRegionFactory.extractFromTexture(texBgCloud);
			texBgCloud.load();

			final ITexture texBgLand = new AssetBitmapTexture(this.getTextureManager(), this.getAssets(), "gfx/bg-land.png");
			this.mTexRegBgLand = TextureRegionFactory.extractFromTexture(texBgLand);
			texBgLand.load();
			
			this.mFont = FontFactory.createFromAsset(
					this.getFontManager(), 
					this.getTextureManager(), 
					256, 
					256, 
					this.getAssets(),
					"font/GILSANUB.TTF", 60, true,
					android.graphics.Color.BLACK);
			this.mFont.load();
			
			final ITexture texItemMenuPlay = new AssetBitmapTexture(this.getTextureManager(), this.getAssets(), "gfx/menu-play.png");
			this.mTexRegItemMenuPlay = TextureRegionFactory.extractFromTexture(texItemMenuPlay);
			texItemMenuPlay.load();
			
			final ITexture texItemMenuAbout = new AssetBitmapTexture(this.getTextureManager(), this.getAssets(), "gfx/menu-about.png");
			this.mTexRegItemMenuAbout = TextureRegionFactory.extractFromTexture(texItemMenuAbout);
			texItemMenuAbout.load();
			
			final ITexture texItemMenuExit = new AssetBitmapTexture(this.getTextureManager(), this.getAssets(), "gfx/menu-exit.png");
			this.mTexRegItemMenuExit = TextureRegionFactory.extractFromTexture(texItemMenuExit);
			texItemMenuExit.load();
        } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    @Override
    protected Scene onCreateScene() {
    	level = 1;
    	state = 0;
    	
    	return createMenu();
    }
    
    private Scene createMenu() {
    	Scene menu = new Scene();
    	
    	final Sprite spriteBgSky = new Sprite(0, 0, this.mTexRegBgSky, this.getVertexBufferObjectManager());
		spriteBgSky.setWidth(CAMERA_WIDTH);
		spriteBgSky.setHeight(CAMERA_HEIGHT);
		menu.attachChild(spriteBgSky);

		final Sprite spriteBgCloud = new Sprite(0, 0, this.mTexRegBgCloud, this.getVertexBufferObjectManager());
		spriteBgCloud.setWidth(CAMERA_WIDTH);
		spriteBgCloud.setHeight(CAMERA_HEIGHT);
		menu.attachChild(spriteBgCloud);

		final Sprite spriteBgLand = new Sprite(0, 0, this.mTexRegBgLand, this.getVertexBufferObjectManager());
		spriteBgLand.setWidth(CAMERA_WIDTH);
		spriteBgLand.setHeight(CAMERA_HEIGHT);
		menu.attachChild(spriteBgLand);
		
		final Rectangle recMenuBg = new Rectangle(0, 0, CAMERA_WIDTH/4, CAMERA_HEIGHT/2, this.getVertexBufferObjectManager());
		recMenuBg.setPosition(CAMERA_WIDTH/2-recMenuBg.getWidth()/2, CAMERA_HEIGHT/2-recMenuBg.getHeight()/2);
		recMenuBg.setColor(128, 90, 96);
//		menu.attachChild(recMenuBg);
		
		final Text textMenuTitle = new Text(0, 0, this.mFont, "MENU", this.getVertexBufferObjectManager());
		textMenuTitle.setPosition(CAMERA_WIDTH/2-textMenuTitle.getWidth()/2, recMenuBg.getY()+10);
		menu.attachChild(textMenuTitle);
		
		final Sprite spriteItemMenuPlay = new Sprite(0, 0, this.mTexRegItemMenuPlay, this.getVertexBufferObjectManager()){
		    @Override
		    public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
		    	if(pSceneTouchEvent.getAction() == TouchEvent.ACTION_DOWN){
                    // you can implement your action
		    		mEngine.setScene(runGame());
		    		state = 1;
	    		}

                return true;
			}
		};
		menu.registerTouchArea(spriteItemMenuPlay);
		spriteItemMenuPlay.setPosition(CAMERA_WIDTH/2-spriteItemMenuPlay.getWidth()/2, recMenuBg.getY()+110);
		menu.attachChild(spriteItemMenuPlay);
		
//		final Sprite spriteItemMenuAbout = new Sprite(0, 0, this.mTexRegItemMenuAbout, this.getVertexBufferObjectManager());
//		spriteItemMenuAbout.setPosition(CAMERA_WIDTH/2-spriteItemMenuAbout.getWidth()/2, recMenuBg.getY()+180);
//		menu.attachChild(spriteItemMenuAbout);
		
		final Sprite spriteItemMenuExit = new Sprite(0, 0, this.mTexRegItemMenuExit, this.getVertexBufferObjectManager()){
		    @Override
		    public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
		    	if(pSceneTouchEvent.getAction() == TouchEvent.ACTION_DOWN){
                    // you can implement your action
		    		System.exit(0);
		    		state = 0;
	    		}

                return true;
			}
		};
		menu.registerTouchArea(spriteItemMenuExit);
		spriteItemMenuExit.setPosition(CAMERA_WIDTH/2-spriteItemMenuExit.getWidth()/2, recMenuBg.getY()+180);
		menu.attachChild(spriteItemMenuExit);
    	
    	return menu;
    }
    
    private Scene runGame() {
    	this.gameLevel = new GameLevel(level, CAMERA_WIDTH, CAMERA_HEIGHT, this.getVertexBufferObjectManager());
    	final Scene scene = this.gameLevel.scene();
    	
    	this.gameLevel.onFinished(new Callable<Void>() {
		   public Void call() {
			   	level++;
			   	final Scene scene = runGame();
			   	mEngine.setScene(scene);
				return null;
		   }
		});
    	return scene;
    }
    
    @Override
    public void onBackPressed(){
    	// Back to menu!!!
    	if(state == 1){
    		mEngine.setScene(createMenu());
    		state = 0;
    	} else{
    		System.exit(0);
    	}
    }
}