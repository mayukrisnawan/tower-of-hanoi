package com.example.thetowerofhanoiec;

import java.io.IOException;

import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.font.FontManager;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.TextureManager;
import org.andengine.opengl.texture.bitmap.AssetBitmapTexture;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.texture.region.TextureRegionFactory;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import android.content.res.AssetManager;

public class Menu {
	
	private static int CAMERA_WIDTH = 1024, CAMERA_HEIGHT = 600;
	
	private static TextureManager textureManager;
	private static AssetManager assetManager;
	private static FontManager fontManager;
	
	private static TextureRegion mTexRegBgSky;
	private static TextureRegion mTexRegBgCloud;
	private static TextureRegion mTexRegBgLand;
	private static Font mFont;
	private static TextureRegion mTexRegItemMenuPlay;
	private static TextureRegion mTexRegItemMenuAbout;
	
	private static TextureManager getTextureManager(){
		return Menu.textureManager;
	}
	private static AssetManager getAssets(){
		return Menu.assetManager;
	}
	private static FontManager getFontManager(){
		return Menu.fontManager;
	}
	
	public static void registerResources(TextureManager textureManager, AssetManager assetManager, FontManager fontManager) {
		Menu.textureManager = textureManager;
		Menu.assetManager = assetManager;
		Menu.fontManager = fontManager;
		
		// TODO Auto-generated method stub
		try {
        	final ITexture texBgSky = new AssetBitmapTexture(Menu.getTextureManager(), Menu.getAssets(), "gfx/bg-sky.png");
        	Menu.mTexRegBgSky = TextureRegionFactory.extractFromTexture(texBgSky);
			texBgSky.load();

			final ITexture texBgCloud = new AssetBitmapTexture(Menu.getTextureManager(), Menu.getAssets(), "gfx/bg-cloud.png");
			Menu.mTexRegBgCloud = TextureRegionFactory.extractFromTexture(texBgCloud);
			texBgCloud.load();

			final ITexture texBgLand = new AssetBitmapTexture(Menu.getTextureManager(), Menu.getAssets(), "gfx/bg-land.png");
			Menu.mTexRegBgLand = TextureRegionFactory.extractFromTexture(texBgLand);
			texBgLand.load();
			
			Menu.mFont = FontFactory.createFromAsset(
					Menu.getFontManager(), 
					Menu.getTextureManager(), 
					256, 
					256, 
					Menu.getAssets(),
					"font/GILSANUB.TTF", 60, true,
					android.graphics.Color.BLACK);
			Menu.mFont.load();
			
			final ITexture texItemMenuPlay = new AssetBitmapTexture(Menu.getTextureManager(), Menu.getAssets(), "gfx/menu-play.png");
			Menu.mTexRegItemMenuPlay = TextureRegionFactory.extractFromTexture(texItemMenuPlay);
			texItemMenuPlay.load();
			
			final ITexture texItemMenuAbout = new AssetBitmapTexture(Menu.getTextureManager(), Menu.getAssets(), "gfx/menu-about.png");
			Menu.mTexRegItemMenuAbout = TextureRegionFactory.extractFromTexture(texItemMenuAbout);
			texItemMenuAbout.load();
        } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static Scene createMenu() {
    	Scene menu = new Scene();
    	
    	final Sprite spriteBgSky = new Sprite(0, 0, Menu.mTexRegBgSky, Menu.getVertexBufferObjectManager());
		spriteBgSky.setWidth(CAMERA_WIDTH);
		spriteBgSky.setHeight(CAMERA_HEIGHT);
		menu.attachChild(spriteBgSky);

		final Sprite spriteBgCloud = new Sprite(0, 0, Menu.mTexRegBgCloud, Menu.getVertexBufferObjectManager());
		spriteBgCloud.setWidth(CAMERA_WIDTH);
		spriteBgCloud.setHeight(CAMERA_HEIGHT);
		menu.attachChild(spriteBgCloud);

		final Sprite spriteBgLand = new Sprite(0, 0, Menu.mTexRegBgLand, Menu.getVertexBufferObjectManager());
		spriteBgLand.setWidth(CAMERA_WIDTH);
		spriteBgLand.setHeight(CAMERA_HEIGHT);
		menu.attachChild(spriteBgLand);
		
		final Rectangle recMenuBg = new Rectangle(0, 0, CAMERA_WIDTH/4, CAMERA_HEIGHT/2, Menu.getVertexBufferObjectManager());
		recMenuBg.setPosition(CAMERA_WIDTH/2-recMenuBg.getWidth()/2, CAMERA_HEIGHT/2-recMenuBg.getHeight()/2);
		recMenuBg.setColor(128, 90, 96);
		menu.attachChild(recMenuBg);
		
		final Text textMenuTitle = new Text(0, 0, Menu.mFont, "MENU", Menu.getVertexBufferObjectManager());
		textMenuTitle.setPosition(CAMERA_WIDTH/2-textMenuTitle.getWidth()/2, recMenuBg.getY()+10);
		menu.attachChild(textMenuTitle);
		
		final Sprite spriteItemMenuPlay = new Sprite(0, 0, Menu.mTexRegItemMenuPlay, Menu.getVertexBufferObjectManager());
		spriteItemMenuPlay.setPosition(CAMERA_WIDTH/2-spriteItemMenuPlay.getWidth()/2, recMenuBg.getY()+110);
		menu.attachChild(spriteItemMenuPlay);
		
		final Sprite spriteItemMenuAbout = new Sprite(0, 0, Menu.mTexRegItemMenuAbout, Menu.getVertexBufferObjectManager());
		spriteItemMenuAbout.setPosition(CAMERA_WIDTH/2-spriteItemMenuAbout.getWidth()/2, recMenuBg.getY()+180);
		menu.attachChild(spriteItemMenuAbout);
    	
    	return menu;
    }
	private static VertexBufferObjectManager getVertexBufferObjectManager() {
		// TODO Auto-generated method stub
		return null;
	}

}