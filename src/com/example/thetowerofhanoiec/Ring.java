package com.example.thetowerofhanoiec;

import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import android.util.Log;

import java.util.Stack;

public class Ring extends Sprite {
    private int index;
    private int towerIndex;
    public int order;
    public Sprite mSpriteTower;
    public Stack<Ring> stack;

    public Ring(int index, int total, float pX, float pY, ITextureRegion pTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager) {
        super(pX, pY, pTextureRegion, pVertexBufferObjectManager);
        this.index = index;
        this.order = total - index;
    }
    
    public static Ring create(int index, final GameLevel gameLevel){
    	float ringWidth = GameLevel.mTexRegRing().getWidth()*(gameLevel.ringCount()-index)/gameLevel.ringCount();
    	float towerHeight = GameLevel.mTexRegTower().getHeight()-20;
    	float ringHeight = towerHeight/gameLevel.ringCount();
		final Ring ring = new Ring(
			index,
			gameLevel.ringCount(),
			gameLevel.firstQuarterX()-ringWidth/2+ GameLevel.mTexRegTower().getWidth()/2,
			gameLevel.bgWallPosY() - (index+1)* ringHeight,
			GameLevel.mTexRegRing(),
			gameLevel.vertexBufferObjectManager()
		){
		    @Override
		    public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
		    	if (((Ring) this.stack.peek()).order() != this.order())
		            return false;
		        this.setPosition(pSceneTouchEvent.getX() - this.getWidth() / 2, pSceneTouchEvent.getY() - this.getHeight() / 2);
		        if (pSceneTouchEvent.getAction() == TouchEvent.ACTION_UP) {
		        	gameLevel.checkCollisionWithTowers(this);
		        	gameLevel.checkGameFinished(this);
		        }
		        return true;
			}
		};
		ring.setWidth(ringWidth);
		ring.setHeight(ringHeight);
		return ring;
    }
    
    public int index() {
    	return this.index;
    }
    
    public int order() {
    	return this.order;
    }
    
}