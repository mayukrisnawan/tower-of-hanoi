package com.example.thetowerofhanoiec;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Stack;
import java.util.concurrent.Callable;

import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.font.FontManager;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.TextureManager;
import org.andengine.opengl.texture.bitmap.AssetBitmapTexture;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.TextureRegionFactory;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import android.content.res.AssetManager;

public class GameLevel {
	private static ITextureRegion mTexRegBgLand, mTexRegBgSky, mTexRegBgCloud,
			mTexRegBgWall, mTexRegTower, mTexRegRing;
	private static Font mFont;

	private Sprite mSpriteTower1, mSpriteTower2, mSpriteTower3;
	private Stack<Ring> mStack1, mStack2, mStack3;
	private int ringCount, level;
	private int CAMERA_WIDTH, CAMERA_HEIGHT;
	private float centerX, centerY, _1stQuarterX, _3rdQuarterX, bgWallHeight,
			bgWallPosY;
	private VertexBufferObjectManager vertexBufferObjectManager;
	private Scene scene;

	private ArrayList<Ring> rings;
	private Callable<Void> finishedCallback;

	public GameLevel(int level, int CAMERA_WIDTH, int CAMERA_HEIGHT,
			VertexBufferObjectManager vertexBufferObjectManager) {

		this.finishedCallback = null;
		this.level = level;
		this.ringCount = level + 2;
		this.CAMERA_WIDTH = CAMERA_WIDTH;
		this.CAMERA_HEIGHT = CAMERA_HEIGHT;
		this.vertexBufferObjectManager = vertexBufferObjectManager;

		this.centerX = this.CAMERA_WIDTH / 2;
		this.centerY = this.CAMERA_HEIGHT / 2;
		this._1stQuarterX = this.centerX / 2;
		this._3rdQuarterX = this.centerX + (this.centerX / 2);
		this.bgWallHeight = 150;
		this.bgWallPosY = this.CAMERA_HEIGHT - this.bgWallHeight;

		this.mStack1 = new Stack<Ring>();
		this.mStack2 = new Stack<Ring>();
		this.mStack3 = new Stack<Ring>();
		this.rings = new ArrayList<Ring>();

		this.scene = new Scene();
		this.createSprites();
		this.createRings();
	}

	public Scene scene() {
		return this.scene;
	}

	public static ITextureRegion mTexRegTower() {
		return GameLevel.mTexRegTower;
	}

	public static ITextureRegion mTexRegRing() {
		return GameLevel.mTexRegRing;
	}

	public static Font mFont() {
		return GameLevel.mFont;
	}

	public int ringCount() {
		return this.ringCount;
	}

	public float firstQuarterX() {
		return this._1stQuarterX;
	}

	public float bgWallPosY() {
		return this.bgWallPosY;
	}

	public VertexBufferObjectManager vertexBufferObjectManager() {
		return this.vertexBufferObjectManager;
	}

	private void createRings() {
		for (int i = 0; i < this.ringCount; i++) {
			Ring ring = Ring.create(i, this);
			rings.add(ring);
			scene.attachChild(ring);
			scene.registerTouchArea(ring);
			this.mStack1.add(ring);
			ring.stack = this.mStack1;
			ring.mSpriteTower = this.mSpriteTower1;
		}
		scene.setTouchAreaBindingOnActionDownEnabled(true);
	}

	private void createSprites() {

		final Sprite spriteBgSky = new Sprite(0, 0, GameLevel.mTexRegBgSky,
				this.vertexBufferObjectManager);
		spriteBgSky.setWidth(this.CAMERA_WIDTH);
		spriteBgSky.setHeight(this.CAMERA_HEIGHT);
		this.scene.attachChild(spriteBgSky);

		final Sprite spriteBgCloud = new Sprite(0, 0, GameLevel.mTexRegBgCloud,
				this.vertexBufferObjectManager);
		spriteBgCloud.setWidth(this.CAMERA_WIDTH);
		spriteBgCloud.setHeight(this.CAMERA_HEIGHT);
		this.scene.attachChild(spriteBgCloud);

		final Sprite spriteBgLand = new Sprite(0, 0, GameLevel.mTexRegBgLand,
				this.vertexBufferObjectManager);
		spriteBgLand.setWidth(this.CAMERA_WIDTH);
		spriteBgLand.setHeight(this.CAMERA_HEIGHT);
		this.scene.attachChild(spriteBgLand);

		final Sprite spriteBgWall = new Sprite(0, this.bgWallPosY,
				GameLevel.mTexRegBgWall, this.vertexBufferObjectManager);
		spriteBgWall.setWidth(this.CAMERA_WIDTH);
		spriteBgWall.setHeight(this.bgWallHeight);
		this.scene.attachChild(spriteBgWall);

		this.mSpriteTower1 = new Sprite(this._1stQuarterX, this.bgWallPosY
				- GameLevel.mTexRegTower.getHeight(), GameLevel.mTexRegTower,
				this.vertexBufferObjectManager);
		this.scene.attachChild(this.mSpriteTower1);
		this.mSpriteTower2 = new Sprite(this.centerX, this.bgWallPosY
				- GameLevel.mTexRegTower.getHeight(), GameLevel.mTexRegTower,
				this.vertexBufferObjectManager);
		this.scene.attachChild(this.mSpriteTower2);
		this.mSpriteTower3 = new Sprite(this._3rdQuarterX, this.bgWallPosY
				- GameLevel.mTexRegTower.getHeight(), GameLevel.mTexRegTower,
				this.vertexBufferObjectManager);
		this.scene.attachChild(this.mSpriteTower3);

		final Text levelText = new Text(5, 5, GameLevel.mFont, "LEVEL " +  String.valueOf(level),
				this.vertexBufferObjectManager);
		this.scene.attachChild(levelText);
	}

	public static void registerResources(TextureManager textureManager,
			AssetManager assetManager, FontManager fontManager) {
		// TODO Auto-generated method stub
		try {
			final ITexture texBgSky = new AssetBitmapTexture(textureManager,
					assetManager, "gfx/bg-sky.png");
			GameLevel.mTexRegBgSky = TextureRegionFactory
					.extractFromTexture(texBgSky);
			texBgSky.load();

			final ITexture texBgCloud = new AssetBitmapTexture(textureManager,
					assetManager, "gfx/bg-cloud.png");
			GameLevel.mTexRegBgCloud = TextureRegionFactory
					.extractFromTexture(texBgCloud);
			texBgCloud.load();

			final ITexture texBgLand = new AssetBitmapTexture(textureManager,
					assetManager, "gfx/bg-land.png");
			GameLevel.mTexRegBgLand = TextureRegionFactory
					.extractFromTexture(texBgLand);
			texBgLand.load();

			final ITexture texBgWall = new AssetBitmapTexture(textureManager,
					assetManager, "gfx/bg-wall.png");
			GameLevel.mTexRegBgWall = TextureRegionFactory
					.extractFromTexture(texBgWall);
			texBgWall.load();

			final ITexture texTower = new AssetBitmapTexture(textureManager,
					assetManager, "gfx/tower.png");
			GameLevel.mTexRegTower = TextureRegionFactory
					.extractFromTexture(texTower);
			texTower.load();

			final ITexture texRing = new AssetBitmapTexture(textureManager,
					assetManager, "gfx/ring.png");
			GameLevel.mTexRegRing = TextureRegionFactory
					.extractFromTexture(texRing);
			texRing.load();

			GameLevel.mFont = FontFactory.createFromAsset(fontManager,
					textureManager, 256, 256, assetManager,
					"font/GILSANUB.TTF", 60, true,
					android.graphics.Color.BLACK);
			mFont.load();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void checkCollisionWithTowers(Ring ring) {
		Stack<Ring> stack = null;
		Sprite tower = null;
		if (ring.collidesWith(mSpriteTower1)
				&& (mStack1.size() == 0 || ring.order < ((Ring) mStack1.peek()).order)) {
			stack = mStack1;
			tower = mSpriteTower1;
		} else if (ring.collidesWith(mSpriteTower2)
				&& (mStack2.size() == 0 || ring.order < ((Ring) mStack2.peek()).order)) {
			stack = mStack2;
			tower = mSpriteTower2;
		} else if (ring.collidesWith(mSpriteTower3)
				&& (mStack3.size() == 0 || ring.order < ((Ring) mStack3.peek()).order)) {
			stack = mStack3;
			tower = mSpriteTower3;
		} else {
			stack = ring.stack;
			tower = ring.mSpriteTower;
		}
		ring.stack.remove(ring);
		if (stack != null && tower != null && stack.size() == 0) {
			ring.setPosition(
					tower.getX() + tower.getWidth() / 2 - ring.getWidth() / 2,
					tower.getY() + tower.getHeight() - ring.getHeight());
		} else if (stack != null && tower != null && stack.size() > 0) {
			ring.setPosition(
					tower.getX() + tower.getWidth() / 2 - ring.getWidth() / 2,
					((Ring) stack.peek()).getY() - ring.getHeight());
		}
		stack.add(ring);
		ring.stack = stack;
		ring.mSpriteTower = tower;
	}

	public void checkGameFinished(Ring selectedRing) {
		boolean finished = true;
		for (int i = 0; i < this.rings.size(); i++) {
			Ring ring = this.rings.get(i);
			if (ring.mSpriteTower != this.mSpriteTower3) {
				finished = false;
				break;
			}
		}

		if (finished) {
			this.promptGameFinishedMsg();
			this.scene.unregisterTouchArea(selectedRing);
			this.scene.setOnSceneTouchListener(new IOnSceneTouchListener() {
				@Override
				public boolean onSceneTouchEvent(Scene pScene,
						TouchEvent pSceneTouchEvent) {
					finished();
					return false;
				}
			});
		}
	}

	public void onFinished(Callable<Void> callback) {
		this.finishedCallback = callback;
	}

	public void finished() {
		if (this.finishedCallback != null) {
			try {
				this.finishedCallback.call();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void promptGameFinishedMsg() {
		final Text winText = new Text(0, 0, GameLevel.mFont(), "You win!",
				this.vertexBufferObjectManager);
		final Text continueText = new Text(0, 0, GameLevel.mFont(),
				"tap to continue...", this.vertexBufferObjectManager);
		winText.setPosition(centerX - (winText.getWidth() / 2), centerY / 2
				- (winText.getHeight()) - continueText.getHeight() - 10);
		continueText.setPosition(centerX - (continueText.getWidth() / 2),
				centerY / 2 - (continueText.getHeight()));
		this.scene.attachChild(winText);
		this.scene.attachChild(continueText);
	}
}
